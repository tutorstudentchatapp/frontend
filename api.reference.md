# API Reference

(auth) after request name means `Authorization` should contain JWT token

## Authorize

`POST /auth/login`

Request

```
{
    "login": string,
    "password": string
}
```

Response
```
{
    "token": string
}
```

## Get Available Channels (auth)

`GET /channels`

Response
```
{
    channels: [
        {
            "name": string,
            "id": number,
            "unreadMessageCount": number,
            "type": Direct | Group,
            "properties": {
                ...
            }
        }
    ]
}
```

## Get Chat Messages For Chat (auth)

`GET /channels/{id}/messages`

Response
```
{
    "messages": [
        "senderId": number,
        "text": string,
        "date": Date,
        "deliveredToIds": [ number ],
        "readByIds": [ number ]
    ]
}
```

## Get Student Info

`GET users/{id}`

Response
```
{
    "fullName": string,
    "avatarUrl": string,
    "lastOnline": Date,
    "groupName": string
}
```

## Update Message Status (auth)
Changes status of message from sent to received. If called again, changes from received to viewed.

`PATCH messages/{id}/status`

Response
```
{
    "id": number,
    "status": sent | received | viewed
}
```

# Websocket communication
Following methods are desired:
1. Send message
Request
```
{
    "text": string,
    "chatId": number
}
```
2. Receive notification if new message is sent
3. Receive notification if user connected to chat or disconnected
4. Get list of online users ATM
