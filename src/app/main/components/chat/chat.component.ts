import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Chat} from '../../state/chat.model';
import {ChatsQuery} from '../../state/chats.query';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  @ViewChild('chatList')
  private chatListRef: ElementRef;

  public activeChat: Chat;

  constructor(private chatsQuery: ChatsQuery) { }

  ngOnInit(): void {
    setTimeout(() => this.scrollBottom());
    this.chatsQuery.selectActiveChat().subscribe(c => this.activeChat = c);
  }

  private scrollBottom(): void {
    console.log(this.chatListRef);
    const native = this.chatListRef.nativeElement;

    native.scrollTop = native.scrollHeight;
  }

}
