import { Component, OnInit } from '@angular/core';
import {ChatsQuery} from '../../state/chats.query';
import {Chat} from '../../state/chat.model';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  public activeChat: Chat;

  constructor(private chatsQuery: ChatsQuery) { }

  ngOnInit(): void {
    this.chatsQuery.selectActiveChat().subscribe(c => this.activeChat = c);
  }

}
