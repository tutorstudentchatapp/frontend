import {Component, HostListener, Input, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  @Input()
  public type: MessageType = 'Sent';

  @Input()
  public senderName: string;

  @ViewChild('contextMenuTemplate')
  private contextMenuRef: TemplateRef<any>;

  overlayRef: OverlayRef | null;

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef
  ) { }

  ngOnInit(): void {
  }

  public openContextMenu({ x, y }: MouseEvent): void {
    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo({ x, y })
      .withPositions([
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        }
      ]);

    this.overlayRef = this.overlay.create({
      positionStrategy,
      scrollStrategy: this.overlay.scrollStrategies.close()
    });

    console.log(positionStrategy);

    this.overlayRef.attach(new TemplatePortal(this.contextMenuRef, this.viewContainerRef, {}));
  }

}

export type MessageType = 'Sent' | 'Received';
