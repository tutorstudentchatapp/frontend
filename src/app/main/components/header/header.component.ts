import { Component, OnInit } from '@angular/core';
import {ChatsQuery} from '../../state/chats.query';
import {Chat} from '../../state/chat.model';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public activeChat: Chat;

  constructor(private chatsQuery: ChatsQuery) { }

  ngOnInit(): void {
    this.chatsQuery.selectActiveChat().subscribe(c => this.activeChat = c);
  }

}
