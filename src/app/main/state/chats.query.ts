import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ChatsStore, ChatsState } from './chats.store';
import {Observable} from 'rxjs';
import {Chat} from './chat.model';
import {map} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ChatsQuery extends QueryEntity<ChatsState> {

  constructor(protected store: ChatsStore) {
    super(store);
  }

  public selectActiveChat(): Observable<Chat> {
    return this.selectAll().pipe(map(q => q.filter(c => c.active)[0]));
  }

}
