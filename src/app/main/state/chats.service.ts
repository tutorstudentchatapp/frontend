import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { ChatsStore } from './chats.store';
import { Chat } from './chat.model';
import { tap } from 'rxjs/operators';
import {Observable, of} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ChatsService {

  constructor(private chatsStore: ChatsStore,
              private http: HttpClient) {
  }

  public get(): Observable<Chat[]> {
    return this.http.get<Chat[]>('https://api.com').pipe(tap(entities => {
      this.chatsStore.set(entities);
    }));
  }

  public add(chat: Chat): void {
    this.chatsStore.add(chat);
  }

  public update(id, chat: Partial<Chat>): void {
    this.chatsStore.update(id, chat);
  }

  public remove(id: ID): void {
    this.chatsStore.remove(id);
  }

  public selectActive(id: number): void {
    const ids = this.chatsStore.getValue().ids as number[];
    this.chatsStore.update(ids, { active: false });

    this.chatsStore.update(id, { active: true });
  }
}
