import {Injectable} from '@angular/core';
import {Chat} from './chat.model';
import {EntityState, EntityStore, StoreConfig} from '@datorama/akita';

export interface ChatsState extends EntityState<Chat> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'chats' })
export class ChatsStore extends EntityStore<ChatsState> {

  constructor() {
    super();

    this.add(ChatsStore.createDefaultChats());
  }

  private static createDefaultChats(): Chat[] {
    return [
      {id: 1, name: 'Студент Студент', active: true, groupName: 'АВТ-712'},
      {id: 2, name: 'Другой Тьютор', active: false}
    ];
  }

}

