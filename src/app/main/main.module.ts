import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './components/main/main.component';
import { HeaderComponent } from './components/header/header.component';
import { ChatComponent } from './components/chat/chat.component';
import { DetailsComponent } from './components/details/details.component';
import { MessageInputComponent } from './components/message-input/message-input.component';
import { MessageComponent } from './components/message/message.component';
import { EditorModule } from 'primeng/editor';
import {ContextMenuModule} from 'primeng/contextmenu';
import {QuillModule} from 'ngx-quill';



@NgModule({
  declarations: [MainComponent, HeaderComponent, ChatComponent, DetailsComponent, MessageInputComponent, MessageComponent],
  exports: [
    MainComponent
  ],
  imports: [
    CommonModule,
    EditorModule,
    ContextMenuModule,
    QuillModule.forRoot()
  ]
})
export class MainModule { }
