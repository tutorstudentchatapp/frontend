import { Component, OnInit } from '@angular/core';
import {Chat} from '../../../main/state/chat.model';
import {ChatsQuery} from '../../../main/state/chats.query';
import {ChatsService} from '../../../main/state/chats.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  public availableChats: Chat[] = [];

  constructor(private chatsQuery: ChatsQuery, private chatsService: ChatsService) { }

  ngOnInit(): void {
    this.chatsQuery.selectAll().subscribe(q => this.availableChats = q);
  }

  public selectAnotherActiveChat(chatId: number): void {
    this.chatsService.selectActive(chatId);
  }

}
