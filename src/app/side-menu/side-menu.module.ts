import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';



@NgModule({
  declarations: [HeaderComponent, SideMenuComponent, ContactListComponent],
  exports: [
    SideMenuComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SideMenuModule { }
