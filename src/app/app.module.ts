import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {Route, RouterModule} from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import {SideMenuModule} from './side-menu/side-menu.module';
import {MainModule} from './main/main.module';
import {OverlayModule} from '@angular/cdk/overlay';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { environment } from '../environments/environment';
import {HttpClientModule} from '@angular/common/http';

const routes: Route[] = [

];


@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    SideMenuModule,
    MainModule,
    OverlayModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
